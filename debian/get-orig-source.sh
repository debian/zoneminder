#!/bin/bash
: <<=cut

=head1 DESCRIPTION

This script is called by uscan(1) as per "debian/watch" to export git
submodules as Multi Upstream Tarball (MUT) components.

=head1 COPYRIGHT

Copyright: 2018 Dmitry Smirnov <onlyjob@member.fsf.org>

=head1 LICENSE

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

=cut

set -e
set -u

if [ "$1" = '--upstream-version' ]; then
    version="$2"
else
    printf "E: missing argument '--upstream-version'.\n" 1>&2
    exit 1
fi

GIT_RURL="https://github.com/ZoneMinder/zoneminder.git"
export XZ_OPT="-6v"
DEB_SOURCE="$( dpkg-parsechangelog -SSource )"
#DEB_VERSION="$( dpkg-parsechangelog -SVersion )"
filename="$( readlink -f ../${DEB_SOURCE}_${version}.orig.tar.xz )"
[ -s "${filename}" ] || exit 1

uversion="${version%%?dfsg*}"
work_dir="$( mktemp -d -t get-orig-source_${DEB_SOURCE}_XXXXXXXX )"
trap "rm -rf '${work_dir}'" EXIT
git clone --branch ${uversion} --depth 1 --recursive ${GIT_RURL} "${work_dir}"

## fetch submodules/components:
for I in $(cd "${work_dir}" && git submodule foreach --quiet 'echo $path'); do
    printf ":: Processing ${I}\n" 1>&2
    component=${I##*/}
    FN="$( readlink -f ../${DEB_SOURCE}_${version}.orig-${component}.tar.gz )"
    if [ ! -s "${FN}" ]; then
        ( cd "${work_dir}"/"${I}"/.. && tar -caf "${FN}" "${component}" )

        mk-origtargz --package ${DEB_SOURCE} --version ${version} \
          --rename --repack --compression xz --directory .. \
          --component ${component} --copyright-file debian/copyright \
        "${FN}"
    fi
done
#####

rm -rf "${work_dir}"
